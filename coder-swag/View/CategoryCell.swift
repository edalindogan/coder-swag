//
//  CategoryCell.swift
//  coder-swag
//
//  Created by Jelo Alindogan on 12/12/2017.
//  Copyright © 2017 Emilio Angelo Alindogan. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func updateViews(category: Category) {
        categoryTitle.text = category.title
        categoryImg.image = UIImage(named: category.imageName)
    }
    
}
