//
//  Category.swift
//  coder-swag
//
//  Created by Jelo Alindogan on 13/12/2017.
//  Copyright © 2017 Emilio Angelo Alindogan. All rights reserved.
//

import Foundation

struct Category {
    
    private (set) public var title: String
    private (set) public var imageName: String
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
}
