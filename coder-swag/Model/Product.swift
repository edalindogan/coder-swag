//
//  Product.swift
//  coder-swag
//
//  Created by Jelo Alindogan on 14/12/2017.
//  Copyright © 2017 Emilio Angelo Alindogan. All rights reserved.
//

import Foundation

struct Product {
    private (set) public var title: String
    private (set) public var price: String
    private (set) public var imageName: String
    
    init(title: String, price: String, imageName: String) {
        self.title = title
        self.price = price
        self.imageName = imageName
    }
}
